<?php

declare(strict_types=1);

namespace Application\Exception;


class ReadOnlyResourceException extends Exception
{
}
