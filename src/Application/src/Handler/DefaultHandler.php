<?php

declare(strict_types=1);

namespace Application\Handler;


use Vallarj\Mezzio\WebService\Handler\AbstractJsonApiRequestHandler;

class DefaultHandler extends AbstractJsonApiRequestHandler
{
    /** @var array */
    protected $authorizedMethods = [];

    /**
     * @inheritDoc
     */
    public function handleGetList()
    {
        return (object)[
            "jsonapi" => (object)[
                "version" => "1.0"
            ],
            "meta" => (object)[
                "title" => "Project skeleton for Mezzio Web Service applications",
                "system_time" => date(DATE_ATOM)
            ],
        ];
    }
}
