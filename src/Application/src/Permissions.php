<?php

declare(strict_types=1);

namespace Application;


class Permissions
{
    private const PREFIX = 'app';
    const ANY = self::PREFIX . '.*';

    const USERS = self::PREFIX . '.users.*';
    const USERS_READ = self::PREFIX . '.users.read';
    const USERS_CREATE = self::PREFIX . '.users.create';
    const USERS_UPDATE = self::PREFIX . '.users.update';
    const USERS_ACTIVATE = self::PREFIX . '.users.activate';
    const USERS_DEACTIVATE = self::PREFIX . '.users.deactivate';
    const USERS_RESET_PASSWORD = self::PREFIX . '.users.reset_password';
    const USERS_MANAGE_ROLES = self::PREFIX . '.users.manage_roles';
}
