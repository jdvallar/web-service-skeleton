<?php

declare(strict_types=1);

namespace Application\Schema;


use Application\DTO\UserDTO;
use Vallarj\Mezzio\WebService\Rbac\Schema\AbstractAdminUserSchema;

class AdminUserSchema extends AbstractAdminUserSchema
{
    /** @var string */
    protected $mappingClass = UserDTO::class;

    /** @var string */
    protected $resourceType = 'users';
}
