<?php

declare(strict_types=1);

namespace Application\Schema;


use Application\DTO\UserDTO;
use Vallarj\Mezzio\WebService\Rbac\Schema\AbstractUserSchema;

class UserSchema extends AbstractUserSchema
{
    /** @var string */
    protected $mappingClass = UserDTO::class;

    /** @var string */
    protected $resourceType = 'users';
}
