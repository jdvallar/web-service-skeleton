<?php

declare(strict_types=1);

namespace Application\Entity;


use Doctrine\ORM\Mapping as ORM;
use Vallarj\Laminas\Rbac\Entity\AbstractUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends AbstractUser
{
}
