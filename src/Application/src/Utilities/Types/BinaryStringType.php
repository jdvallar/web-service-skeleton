<?php

declare(strict_types=1);

namespace Application\Utilities\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\BinaryType;

/**
 * Doctrine DBAL Type that maps an SQL BINARY/VARBINARY to a PHP string
 *
 * @package Application\Utilities\Types
 */
class BinaryStringType extends BinaryType
{
    const BINARY_STRING = 'binary_string';

    /**
     * @inheritDoc
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return is_resource($value) ? stream_get_contents($value) : $value;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::BINARY_STRING;
    }

    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
