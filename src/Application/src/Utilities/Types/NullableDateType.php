<?php

declare(strict_types=1);

namespace Application\Utilities\Types;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use Doctrine\DBAL\Types\DateType;

/**
 * Doctrine DBAL Type that maps an SQL DATE to a PHP DateTime object.
 * Addresses the issue where some null date values are converted to the string '0000-00-00'
 */
class NullableDateType extends DateType
{
    const NULLABLE_DATE = 'nullable_date';

    /**
     * @inheritDoc
     * @throws ConversionException
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof \DateTimeInterface) {
            return $value;
        } else if ($value === '0000-00-00') {
            return null;
        }

        $val = \DateTime::createFromFormat('!'.$platform->getDateFormatString(), $value);
        if ( ! $val) {
            throw ConversionException::conversionFailedFormat(
                $value,
                $this->getName(),
                $platform->getDateFormatString()
            );
        }

        return $val;
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return self::NULLABLE_DATE;
    }

    /**
     * @inheritDoc
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }
}
