<?php

declare(strict_types=1);

namespace Application\Utilities\AutoMapper;


class IncludedAssociationBuilder
{
    /**
     * Build a nested association array suitable for AutoMapper
     *
     * @param string[] $associations
     * @return string[]
     */
    public static function build(array $associations)
    {
        // Include all top level attributes
        $includedProperties = ['*'];

        foreach ($associations as $association) {
            // Include the association
            $includedProperties[] = $association;

            // as well as its attributes
            $includedProperties[] = $association . '.*';
        }

        return $includedProperties;
    }
}
