<?php

declare(strict_types=1);

namespace Application\Utilities\Validator;


use Laminas\Validator\AbstractValidator;

class BooleanValidator extends AbstractValidator
{
    const NOT_BOOLEAN = 'booleanNotBoolean';

    /** @var string[] */
    protected $messageTemplates = [
        self::NOT_BOOLEAN => "Invalid value.",
    ];

    /**
     * @inheritDoc
     */
    public function isValid($value)
    {
        if (!is_bool($value)) {
            $this->error(self::NOT_BOOLEAN);
            return false;
        }
        return true;
    }
}
