<?php

declare(strict_types=1);

namespace Application\Utilities\Validator;


use Laminas\Validator\Regex;

class AmountValidator extends Regex
{
    /**
     * AmountValidator constructor.
     *
     * @param mixed $pattern
     */
    public function __construct($pattern)
    {
        parent::__construct('/^[0-9]+(\.[0-9]{1,2})?$/');

        $this->setMessage('Invalid amount.', self::NOT_MATCH);
    }
}
