<?php

declare(strict_types=1);

namespace Application\Utilities\Validator;


use Laminas\Validator\Regex;

class PercentageValidator extends Regex
{
    /**
     * PercentageValidator constructor.
     */
    public function __construct()
    {
        parent::__construct('/^0(\.[0-9]{1,5})?$/');

        $this->setMessage('Invalid PCT value.', self::NOT_MATCH);
    }
}
