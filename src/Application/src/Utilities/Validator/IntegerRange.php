<?php

declare(strict_types=1);

namespace Application\Utilities\Validator;


use Laminas\Validator\AbstractValidator;
use Laminas\Validator\Exception;

class IntegerRange extends AbstractValidator
{
    const NOT_INTEGER = 'integerRangeNotInteger';
    const OUT_OF_RANGE = 'integerRangeOutOfRange';

    /** @var string[] */
    protected $messageTemplates = [
        self::NOT_INTEGER => "Field must be an integer.",
        self::OUT_OF_RANGE => "Value must be between '%min%' and '%max%'"
    ];

    /** @var string[][] */
    protected $messageVariables = [
        'min' => [ 'options' => 'min' ],
        'max' => [ 'options' => 'max' ]
    ];

    /** @var array */
    protected $options = [
        'min' => 0,
        'max' => PHP_INT_MAX
    ];

    /**
     * Sets validator options
     *
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        if (!is_array($options)) {
            $options = func_get_args();
            $temp['min'] = array_shift($options);
            if (!empty($options)) {
                $temp['max'] = array_shift($options);
            }

            $options = $temp;
        }

        parent::__construct($options);
    }

    /**
     * Returns the min option
     *
     * @return int
     */
    public function getMin()
    {
        return $this->options['min'];
    }

    /**
     * Sets the min option
     *
     * @param mixed $min
     * @throws Exception\InvalidArgumentException
     * @return IntegerRange
     */
    public function setMin($min)
    {
        if ($min > $this->getMax()) {
            throw new Exception\InvalidArgumentException(
                "The minimum must be less than or equal to the maximum value, but {$min} > {$this->getMax()}"
            );
        }

        $this->options['min'] = max(0, (int) $min);
        return $this;
    }

    /**
     * Returns the max option
     *
     * @return mixed
     */
    public function getMax()
    {
        return $this->options['max'];
    }

    /**
     * Sets the max option
     *
     * @param mixed $max
     * @throws Exception\InvalidArgumentException
     * @return IntegerRange
     */
    public function setMax($max)
    {
        if (!is_int($max)) {
            throw new Exception\InvalidArgumentException(
                "The maximum option must be an integer."
            );
        } else if ($max < $this->getMin()) {
            throw new Exception\InvalidArgumentException(
                "The maximum must be greater than or equal to the minimum value, but {$max} < {$this->getMin()}"
            );
        } else {
            $this->options['max'] = (int) $max;
        }

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        if (!is_int($value)) {
            $this->error(self::NOT_INTEGER);
            return false;
        }

        $this->setValue($value);

        if ($value < $this->getMin() || $value > $this->getMax()) {
            $this->error(self::OUT_OF_RANGE);
            return false;
        }

        return true;
    }
}
