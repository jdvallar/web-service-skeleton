<?php

declare(strict_types=1);

namespace Application\Repository;


use Application\Entity\User;
use Vallarj\Laminas\Rbac\Repository\AbstractUserRepository;

class UserRepository extends AbstractUserRepository
{
    /**
     * @inheritDoc
     */
    public function getUserClass(): string
    {
        return User::class;
    }
}
