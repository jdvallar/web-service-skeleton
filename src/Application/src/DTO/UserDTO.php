<?php

declare(strict_types=1);

namespace Application\DTO;


use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;

class UserDTO extends AbstractUserDTO
{
}
