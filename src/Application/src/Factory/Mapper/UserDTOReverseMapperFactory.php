<?php

declare(strict_types=1);

namespace Application\Factory\Mapper;


use Application\Mapper\UserDTOReverseMapper;
use Application\Repository\UserRepository;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserDTOReverseMapperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new UserDTOReverseMapper($container->get(UserRepository::class));
    }
}
