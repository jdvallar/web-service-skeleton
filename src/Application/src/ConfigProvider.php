<?php

declare(strict_types=1);

namespace Application;


use Application\Utilities\Types\BinaryStringType;
use Application\Utilities\Types\NullableDateType;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use DoctrineExtensions\Query\Mysql\IfElse;
use Vallarj\ObjectMapper\AutoMapper\Configuration;
use Laminas\ServiceManager\Factory\InvokableFactory;
use Ramsey\Uuid\Doctrine\UuidBinaryOrderedTimeType;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'doctrine' => $this->getDoctrineConfiguration(),
            'php-object-mapper' => $this->getObjectMapperConfiguration(),
            'php-json-api' => $this->getJsonApiConfiguration()
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                // Handlers
                Handler\DefaultHandler::class => InvokableFactory::class,

                // Repositories
                Repository\UserRepository::class => Factory\Repository\UserRepositoryFactory::class,

                // Schemas
                Schema\UserSchema::class => InvokableFactory::class,
                Schema\AdminUserSchema::class => InvokableFactory::class,

                // Mappers
                Mapper\UserDTOForwardMapper::class => InvokableFactory::class,
                Mapper\UserDTOReverseMapper::class => Factory\Mapper\UserDTOReverseMapperFactory::class,
            ],
        ];
    }

    /**
     * Returns Doctrine configuration
     *
     * @return array
     */
    public function getDoctrineConfiguration(): array
    {
        return [
            'configuration' => [
                'orm_default' => [
                    'types' => [
                        UuidBinaryOrderedTimeType::NAME => UuidBinaryOrderedTimeType::class,
                        BinaryStringType::BINARY_STRING => BinaryStringType::class,
                        NullableDateType::NULLABLE_DATE => NullableDateType::class,
                    ],
                    'string_functions' => [
                        'IfElse' => IfElse::class,
                    ],
                ],
            ],
            'driver' => [
                __NAMESPACE__ . '_driver' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [
                        __DIR__ . '/Entity',
                    ],
                ],
                'orm_default' => [
                    'drivers' => [
                        __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                    ],
                ],
            ],
        ];
    }

    /**
     * Returns php-object-mapper configuration
     *
     * @return array
     */
    public function getObjectMapperConfiguration(): array
    {
        return [
            'configuration' => [
                Configuration::MAX_DEPTH => 7
            ],
            'mappers' => [
            ],
        ];
    }

    /**
     * Returns php-json-api configuration
     *
     * @return array
     */
    public function getJsonApiConfiguration(): array
    {
        return [
            'schemas' => [
            ],
        ];
    }
}
