<?php

declare(strict_types=1);

namespace Application\Mapper;


use Application\DTO\UserDTO;
use Application\Entity\User;
use Vallarj\Mezzio\WebService\Rbac\Mapper\AbstractUserDTOReverseMapper;

class UserDTOReverseMapper extends AbstractUserDTOReverseMapper
{
    /** @var string */
    protected $sourceClass = UserDTO::class;

    /** @var string */
    protected $targetClass = User::class;
}
