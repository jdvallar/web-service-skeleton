<?php

declare(strict_types=1);

namespace Application\Mapper;


use Application\DTO\UserDTO;
use Application\Entity\User;
use Vallarj\Mezzio\WebService\Rbac\Mapper\AbstractUserDTOForwardMapper;

class UserDTOForwardMapper extends AbstractUserDTOForwardMapper
{
    /** @var string */
    protected $sourceClass = User::class;

    /** @var string */
    protected $targetClass = UserDTO::class;
}
