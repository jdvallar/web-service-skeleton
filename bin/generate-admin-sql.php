<?php

declare(strict_types=1);

chdir(__DIR__ . '/../');

// Decline non-CLI requests
if (php_sapi_name() !== 'cli') {
    throw new Exception("This script can only be run in CLI mode.");
}

// Composer autoloading
include 'vendor/autoload.php';

$uuidFactory = new \Ramsey\Uuid\UuidFactory();
$codec = new \Ramsey\Uuid\Codec\OrderedTimeCodec($uuidFactory->getUuidBuilder());
$uuidFactory->setCodec($codec);

// Generate UUID
$uuid = $uuidFactory->uuid1();
$id = '0x' . bin2hex($uuid->getBytes());
$idStr = $uuid->toString();

echo "Username: ";
$un = "'" . trim(fgets(STDIN)) . "'";
echo "Password: ";
$pw = trim(fgets(STDIN));
$pwh = "'" . password_hash($pw, PASSWORD_DEFAULT) . "'";
$pw = "'" . $pw . "'";
echo "First name: ";
$fn_raw = trim(fgets(STDIN));
$fn = "'" . $fn_raw . "'";
echo "Middle name (Leave blank if none): ";
$mn_raw = trim(fgets(STDIN));
$mn = $mn_raw ? "'" . $mn_raw . "'" : "NULL";
echo "Last name: ";
$ln_raw = trim(fgets(STDIN));
$ln = "'" . $ln_raw . "'";

echo "\nExecute the following SQL statements to create a super administrator account.\n";
echo "Note that only the default `AbstractUser` fields are included. Modify as needed.\n\n";

$nameIndex = "'" . strtoupper($ln_raw) . "_" . strtoupper($fn_raw) . "_" . strtoupper($mn_raw ?? "") . "'";

echo "-- ID: ${idStr}\n" .
     "INSERT INTO user (id, first_name, middle_name, last_name, username, password, active, " .
     "temporary_password, uses_temporary_password, internal, name_index)\n" .
     "VALUES ($id, $fn, $mn, $ln, $un, $pwh, 1, $pw, 1, 1, $nameIndex);\n\n";

echo "-- Role: Super Administrator (_super_admin)\n" .
     "INSERT INTO _rbac_user_role (user_id, role_id) VALUES ($id, '_super_admin');\n";
