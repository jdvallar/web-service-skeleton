<?php

declare(strict_types=1);

chdir(__DIR__ . '/../config/autoload');

if (!file_exists('doctrine.local.php') && file_exists('doctrine.local.php.dist')) {
    copy('doctrine.local.php.dist', 'doctrine.local.php');
}
if (!file_exists('hydra.local.php') && file_exists('hydra.local.php.dist')) {
    copy('hydra.local.php.dist', 'hydra.local.php');
}


if (isset($argv[1]) && $argv[1] == '--dev') {
    if (
        !file_exists('webservice.dev.local.php')
        && file_exists('webservice.dev.local.php.dist')
    ) {
        copy('webservice.dev.local.php.dist', 'webservice.dev.local.php');
    }

    if (
        !file_exists('rbac-sync.local.php')
        && file_exists('rbac-sync.local.php.dist')
    ) {
        copy('rbac-sync.local.php.dist', 'rbac-sync.local.php');
    }
}
