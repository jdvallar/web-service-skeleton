<?php

declare(strict_types=1);

use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Ramsey\Uuid\UuidFactory;

chdir(__DIR__ . '/../');

// Decline non-CLI requests
if (php_sapi_name() !== 'cli') {
    throw new Exception("This script can only be run in CLI mode.");
}

// Composer autoloading
include 'vendor/autoload.php';

$uuidFactory = new UuidFactory();
$codec = new OrderedTimeCodec($uuidFactory->getUuidBuilder());
$uuidFactory->setCodec($codec);

echo "uuid_binary_ordered_time: ";
$ordered = trim(fgets(STDIN));
$uuid = $uuidFactory->fromString($ordered);

$hex = '0x' . bin2hex($uuid->getBytes());
echo "\nHex:\n${hex}\n";
