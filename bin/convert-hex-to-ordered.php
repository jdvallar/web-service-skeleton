<?php

declare(strict_types=1);

use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Ramsey\Uuid\UuidFactory;

chdir(__DIR__ . '/../');

// Decline non-CLI requests
if (php_sapi_name() !== 'cli') {
    throw new Exception("This script can only be run in CLI mode.");
}

// Composer autoloading
include 'vendor/autoload.php';

$uuidFactory = new UuidFactory();
$codec = new OrderedTimeCodec($uuidFactory->getUuidBuilder());
$uuidFactory->setCodec($codec);

echo "Hex: ";
$hex = hex2bin(preg_replace("/\\s+/", "", trim(fgets(STDIN))));
$uuid = $uuidFactory->fromBytes($hex);

$ordered = $uuid->toString();
echo "\nuuid_binary_ordered_time:\n${ordered}\n";
