<?php

/**
 * Mezzio WebService global configuration
 */

declare(strict_types=1);


return [
    'web-service' => [
        'rbac' => [
            'user-repository' => Application\Repository\UserRepository::class,
            'permissions' => [
                'regular-user' => Application\Permissions::ANY,
                'role-read' => Application\Permissions::USERS,
                'role-manage' => Application\Permissions::USERS_MANAGE_ROLES,
                'user-admin' => Application\Permissions::USERS,
                'user-read' => Application\Permissions::USERS_READ,
                'user-create' => Application\Permissions::USERS_CREATE,
                'user-update' => Application\Permissions::USERS_UPDATE,
                'user-activate' => Application\Permissions::USERS_ACTIVATE,
                'user-deactivate' => Application\Permissions::USERS_DEACTIVATE,
                'user-reset-password' => Application\Permissions::USERS_RESET_PASSWORD
            ],
        ],
        'users-page-count' => 15,
        'entities' => [
            'user' => Application\Entity\User::class,
        ],
        'schemas' => [
            'user' => Application\Schema\UserSchema::class,
            'admin-user' => Application\Schema\AdminUserSchema::class
        ],
        'mappers' => [
            'user-forward-mapper' => Application\Mapper\UserDTOForwardMapper::class,
            'user-reverse-mapper' => Application\Mapper\UserDTOReverseMapper::class,
        ],
        'development' => [
            'username' => "admin",
            "scopes" => [
                "openid",
                "offline_access"
            ]
        ],
    ],
];
