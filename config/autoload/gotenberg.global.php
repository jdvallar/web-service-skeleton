<?php

/**
 * Set-up Gotenberg service configuration
 */

declare(strict_types=1);

return [
    'gotenberg' => [
        'host' => 'gotenberg',
        'port' => '3000'
    ],
];
