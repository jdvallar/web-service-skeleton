<?php

declare(strict_types=1);

use Vallarj\Mezzio\WebService\Factory\Response\InternalServerErrorResponseGeneratorFactory;
use Mezzio\Middleware\ErrorResponseGenerator;

return [
    'dependencies' => [
        'factories' => [
            ErrorResponseGenerator::class => InternalServerErrorResponseGeneratorFactory::class,
        ],
    ],
    'web-service' => [
        'development' => [
            'enabled' => false
        ],
    ],
];
