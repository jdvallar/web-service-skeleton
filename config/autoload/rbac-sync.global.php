<?php

/**
 * Set-up RBAC permissions and roles configuration
 */

declare(strict_types=1);

use Application\Permissions;

return [
    'rbac' => [
        'permissions' => [
            Permissions::USERS_READ => 'Read and search user entries',
            Permissions::USERS_CREATE => 'Create new users',
            Permissions::USERS_UPDATE => 'Update user entries',
            Permissions::USERS_ACTIVATE => 'Activate user entries',
            Permissions::USERS_DEACTIVATE => 'Deactivate user entries',
            Permissions::USERS_RESET_PASSWORD => 'Reset user passwords',
            Permissions::USERS_MANAGE_ROLES => 'Manage user roles and permissions',
        ],
        'roles' => [
            'internal' => [
                '_super_admin' => [
                    'description' => 'Super Administrator',
                    'permissions' => [
                        Permissions::USERS_READ,
                        Permissions::USERS_CREATE,
                        Permissions::USERS_UPDATE,
                        Permissions::USERS_ACTIVATE,
                        Permissions::USERS_DEACTIVATE,
                        Permissions::USERS_RESET_PASSWORD,
                        Permissions::USERS_MANAGE_ROLES,
                    ]
                ],
            ],
            'system' => [
                'system_admin' => [
                    'description' => 'System Administrator',
                    'permissions' => [
                        Permissions::USERS_READ,
                        Permissions::USERS_CREATE,
                        Permissions::USERS_UPDATE,
                        Permissions::USERS_ACTIVATE,
                        Permissions::USERS_DEACTIVATE,
                        Permissions::USERS_RESET_PASSWORD,
                        Permissions::USERS_MANAGE_ROLES,
                    ],
                ],
                'user_manager' => [
                    'description' => 'User Accounts Manager',
                    'permissions' => [
                        Permissions::USERS_READ,
                        Permissions::USERS_CREATE,
                        Permissions::USERS_UPDATE,
                        Permissions::USERS_ACTIVATE,
                        Permissions::USERS_DEACTIVATE,
                        Permissions::USERS_RESET_PASSWORD,
                        Permissions::USERS_MANAGE_ROLES,
                    ],
                ],
            ],
        ],
    ],
];
