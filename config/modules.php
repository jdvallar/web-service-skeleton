<?php

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module config-providers used in the application.
 */

declare(strict_types=1);

return [
    Application\ConfigProvider::class,
];
