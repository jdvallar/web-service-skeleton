<?php

declare(strict_types=1);

use Vallarj\Mezzio\WebService\Router\JsonApiRouter;

/**
 * Setup routes with a single request method:
 *
 * $router->route(
 *     '/endpoint',
 *     Application\Handler\DefaultHandler::class,
 *     'endpoint'
 * );
 *
 * @param JsonApiRouter $router
 */
return static function (JsonApiRouter $router) : void {
    // Default webservice routes
    $router->route(
        '/',
        Application\Handler\DefaultHandler::class,
        'root'
    );
    $router->route(
        '/rbac/permissions',
        Vallarj\Mezzio\WebService\Rbac\Handler\PermissionHandler::class,
        '-rbac-permissions'
    );
    $router->route(
        '/rbac/roles',
        Vallarj\Mezzio\WebService\Rbac\Handler\RoleHandler::class,
        '-rbac-roles'
    );
    $router->route(
        '/rbac/role-types',
        Vallarj\Mezzio\WebService\Rbac\Handler\RoleTypeHandler::class,
        "-rbac-role-types"
    );
    $router->route(
        '/users',
        Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler::class,
        'users'
    );

    // Add custom routes here...
};
