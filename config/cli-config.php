<?php

declare(strict_types=1);

$container = require 'container.php';

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet(
    $container->get('doctrine.entitymanager.orm_default')
);
