FROM composer:1.10 AS composer

FROM php:7.4-apache
COPY --from=composer /usr/bin/composer /usr/bin/composer

ENV TZ=Asia/Manila
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
    libzip-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    zip \
    git

WORKDIR /server

RUN printf '[PHP]\ndate.timezone = "Asia/Manila"\n' > /usr/local/etc/php/conf.d/tzone.ini
RUN printf '[PHP]\nerror_reporting = E_ALL & ~E_USER_DEPRECATED & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE\n' > /usr/local/etc/php/conf.d/error_reporting.ini

RUN mkdir -p /proxy/DoctrineORMModule/Proxy && chown -R www-data:www-data /proxy
RUN a2enmod rewrite \
    && a2enmod headers \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install bcmath opcache mysqli zip pdo pdo_mysql gd
